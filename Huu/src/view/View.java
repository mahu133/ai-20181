/**
 *  Giao dien chinh cua tro choi 
 */
package view;


import java.util.Optional;
import controller.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.BoardState;
import model.ComputerPlayer;



public class View implements EventHandler<ActionEvent> {
	public static final int WIDTH_BOARD = 20;
	public static final int HEIGHT_BOARD = 20;
	public static final int WIDTH_PANE = 1200;
	public static final int HEIGHT_PANE = 700;
	
	private Button btnComputer;
	private Button btnExit;
	private Button btnUndo;
	private Labeled timePlayer1, timePlayer2;
	private BoardState boardState ;
	private ComputerPlayer computer ;
	// lop dieu khien
	Controller controller;
	// mang quan co khi danh
	public Button[][] arrayButtonChess;
	// khung view
	public static Stage primaryStage;

	public View() {
	}

	public void start(Stage primaryStage) {
		try {
			View.primaryStage = primaryStage;
			arrayButtonChess = new Button[WIDTH_BOARD][HEIGHT_BOARD];
			boardState = new BoardState(WIDTH_BOARD, HEIGHT_BOARD);
			computer = new ComputerPlayer(boardState);
			controller = new Controller();
			controller.setView(this);
			controller.setPlayer(computer);
				
			BorderPane borderPane = new BorderPane();
			BorderPane borderPaneRight = new BorderPane();
			menu(borderPaneRight);
			
			GridPane root = new GridPane();
			Scene scene = new Scene(borderPane, WIDTH_PANE, HEIGHT_PANE);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			borderPane.setPadding(new Insets(20));
			borderPane.setCenter(root);
			borderPane.setRight(borderPaneRight);
			// mac dinh player 1 di truoc
			controller.setPlayerFlag(1);
			controller.setTimePlayer(timePlayer1, timePlayer2);
			for (int i = 0; i < WIDTH_BOARD; i++) {
				for (int j = 0; j < HEIGHT_BOARD; j++) {
					Button button = new Button();
					button.setPrefSize(40, 40);
					button.setAccessibleText(i + ";" + j);
					arrayButtonChess[i][j] = button;
					root.add(button, j, i);
					button.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							if (!controller.isEnd()) {
								controller.play(button, arrayButtonChess);
							}
						}
					});
				}
			}
			primaryStage.setScene(scene);
			primaryStage.setTitle("Game caro");
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void menu(BorderPane pane) {
		VBox box = new VBox();
		box.setSpacing(10);

		AnchorPane anchorPaneMenu = new AnchorPane();

		// Computer
		btnComputer = new Button("Play");
		btnComputer.setId("btnMenu");
		btnComputer.setOnAction(this);
		AnchorPane.setTopAnchor(btnComputer, 100.0);
		AnchorPane.setLeftAnchor(btnComputer, 30.0);
		AnchorPane.setRightAnchor(btnComputer, 30.0);
		anchorPaneMenu.getChildren().add(btnComputer);

		
		// Undo
		btnUndo = new Button("Quay lại");
		btnUndo.setId("btnMenu");
		btnUndo.setOnAction(this);
		AnchorPane.setTopAnchor(btnUndo, 200.0);
		AnchorPane.setLeftAnchor(btnUndo, 30.0);
		AnchorPane.setRightAnchor(btnUndo, 30.0);
		anchorPaneMenu.getChildren().add(btnUndo);
		
		// exit
		btnExit = new Button("Thoát");
		btnExit.setId("btnMenu");
		btnExit.setOnAction(this);
		AnchorPane.setTopAnchor(btnExit, 300.0);
		AnchorPane.setLeftAnchor(btnExit, 30.0);
		AnchorPane.setRightAnchor(btnExit, 30.0);
		anchorPaneMenu.getChildren().add(btnExit);
		//
		
		box.getChildren().add(anchorPaneMenu);

		// Bottom
		GridPane gridPaneBottom = new GridPane();
		Labeled namePlayer1 = new Label("Người chơi");
		namePlayer1.setId("nameplayer");
		Labeled namePlayer2 = new Label("Máy");
		namePlayer2.setId("nameplayer");
		gridPaneBottom.add(namePlayer1, 0, 0);
		gridPaneBottom.add(namePlayer2, 1, 0);
		box.getChildren().add(gridPaneBottom);
		//
		GridPane gridPaneBottom1 = new GridPane();
		timePlayer1 = new Label("30");
		timePlayer1.setId("timeplayer");
		timePlayer2 = new Label("30");
		timePlayer2.setId("timeplayer");
		gridPaneBottom1.add(timePlayer1, 0, 10);
		gridPaneBottom1.add(timePlayer2, 1, 10);
		box.getChildren().add(gridPaneBottom1);
		//
		pane.setCenter(box);

	}

	
	
	@Override
	public void handle(ActionEvent e) {
		if (e.getSource() == btnExit) {
			primaryStage.close();
		}

		if (e.getSource() == btnComputer) {
			replayComputer();
		}
		if (e.getSource() == btnUndo) {
			controller.undo(arrayButtonChess);
		}

	}
	// che do dau voi may
	public void replayComputer() {
		
		controller.setEnd(false);
		controller.setTimePlayer(timePlayer1, timePlayer2);
		controller.setPlayer(new ComputerPlayer(new BoardState(WIDTH_BOARD, HEIGHT_BOARD)));
		controller.reset(arrayButtonChess);
		gameMode();
		
	}

	// xet xem ai di truoc
	public void gameMode() {
		Alert gameMode = new Alert(AlertType.CONFIRMATION);
		gameMode.setHeaderText("Bạn có muốn chơi trước không ?");
		Optional<ButtonType> result = gameMode.showAndWait();
		if(result.get() == ButtonType.CANCEL) {
			controller.danhCo(WIDTH_BOARD/2 - 1, HEIGHT_BOARD/2,2, arrayButtonChess);
			int[] AScore = {0,3,28,256,2308}; // 0,9,54,162,1458
			int[] DScore = {0,1,9,85,769};   // 0,3,27,99,729
			computer.setAScore(AScore);
			computer.setDScore(DScore);
			controller.setPlayerFlag(1);
		}
		else {
			controller.setPlayerFlag(1);
		}
	}
}
